# godot-starter

Simple shell script that makes opening godot and vim very easy!!

## Getting Started

Install and Setup __(Read till the end)__
https://github.com/habamax/vim-godot

## Usage
Just run the script using a keybinding and have fun!

## Requirements
As far as i know, the requirements are:
+ notify-osd
+ notify-send
+ godot-mono/godot in PATH
+ nvim/vim
